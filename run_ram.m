% RUN_RAM: Script to run MATLAB RAM using an HLS.tl_grid

% Set up Params struct
Params.BathyFile = 'Stellwagenxyz.mat';
Params.acoustic_model = 'RAM';
Params.RunType = 'ENDPOINTS';
Params.src_depth = 5;
Params.src_freq = 141.615126008184;
Params.src_bw = 1.45330739299611;
Params.SSP_month = 10;
Params.Bot.alphaR = 1593;
Params.Bot.betaR = 0;
Params.Bot.rho = 1.339;
Params.Bot.alphaI = 0.592;
Params.Bot.betaI = 0;
Params.rcv_lat = 42.352517;
Params.rcv_lon = -70.34915;
Params.range_resolution_km= 0.05;
Params.grid_depths = [0 72.3 3];
Params.src_lat = 42.3705034321184;
Params.src_lon = -70.34915;

%Instantiate the tl_grid
tl_grid = HLS.tl_grid(Params);
TLmat = tl_grid.compute;