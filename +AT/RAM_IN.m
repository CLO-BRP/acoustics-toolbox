classdef RAM_IN
    %RAM_IN is the input required for running RAM
    %   Default values of the variables have been provided so that this can
    %   be instantiated without an input file
    
    % Copyright 2015 Cornell Lab of Ornithology
    
    properties
        freq = 25.0              % Source frequency (Hz)
        zs = 40.0               % Source depth (m)
        zr = 30.0               % Receiver depth for tl.line (m)
        rmax = 4000.0           % Maximum range (m)
        dr = 5.0                % Range step (m)
        ndr = 1                 % Range decimation factor for grid (1 = none)
        zmax = 1000.0           % Maximum depth (m)
        dz = 1.0                % Depth grid spacing (m)
        ndz = 2                 % Depth decimation factor for grid (1 = none)
        zmplt = 400.0           % Maximum depth of output to tl.grid
        c0 = 1500.0             % Reference sound speed (m/s)
        np = 8                  % # of Pad� terms
        ns = 1                  % # of stability constraints (1, 2)
        rs = 0                  % Maximum range of stability constraints
        rb = [0 4000 -1]        % Range of bathymetry point (m)
        zb = [200 400 -1]       % Depth at bathymetry point (m)
        zcw = [0 3000 -1]       % Depth of sound speed point (m)
        dcw = [1500 1500 -1]    % Sound speed in water column (m/s)
        zcb = [0 -1]            % Depth of sediment point (m)
        dcb = [1700 -1]         % Sound speed in sediment (m/s)
        zrhob = [0 -1]          % Depth of sediment point (m)
        drhob = [1.5 -1]        % Density of sediment (g/cc)
        zattn = [0 900 1000 -1] % Depth of sediment point (m)
        dattn = [0.5 0.5 10 -1] % Attenuation in sediment (dB/wavelength)
        rp = 25000              % Range of profile update (m)
    end
    
    methods
        function obj = RAM_IN(a)
            if nargin == 0
                return
            elseif isa(a, 'AT.RAM_IN')  % Copy constructor
                obj = a;
                return
            elseif ischar(a) % Input is a file
                if ~exist(a, 'file')
                    warning('File %s not found', a);
                    return
                end
                if strfind(a, '.mat')  % matfile
                    in = load(a);
                    names = fieldnames(in);
                    for n = names'
                        if isa(in.(n{:}), 'AT.RAM_IN')
                            obj = in.(n{:});
                            return
                        end
                    end
                    warning('No AT.RAM_IN found in %s', a);
                elseif strfind(a, '.in')  % ASCII input file
                    obj = readfile(obj, a);
                    return
                end
            elseif isa(a, 'HLS.tl_grid') % Input is a Slice
                obj = read_slice(obj, a);
                return
            end
            warning('RAM_IN didn''t find recognized file type in %s -- using default', a);
        end
        
        function obj = readfile(obj, a)
            fid = fopen(a);
            D = textscan(fid, '%f %f %f %*[^\n]', 1, 'HeaderLines', 1);
            obj.freq = D{1};
            obj.zs = D{2};
            obj.zr = D{3};
            D = textscan(fid, '%f %f %d %*[^\n]', 1);
            obj.rmax = D{1};
            obj.dr = double(D{2});
            obj.ndr = D{3};
            D = textscan(fid, '%f %d %d %f %*[^\n]', 1);
            obj.zmax = D{1};
            obj.dz = double(D{2});
            obj.ndz = D{3};
            obj.zmplt = D{4};
            D = textscan(fid, '%f %d %d %f %*[^\n]', 1);
            obj.c0 = D{1};
            obj.np = D{2};
            obj.ns = D{3};
            obj.rs = D{4};
            [obj.rb, obj.zb] = zread(fid);
            if obj.rb(end) < 0
                obj.rb(end) = 2*obj.rmax;
                obj.zb(end) = obj.zb(end-1);
            end
            [obj.zcw, obj.dcw] = zread(fid);
            [obj.zcb, obj.dcb] = zread(fid);
            [obj.zrhob, obj.drhob] = zread(fid);
            [obj.zattn, obj.dattn] = zread(fid);
            if ~feof(fid)
                D = textscan(fid, '%f %*[^\n]', 1);
                obj.rp = D{1};
            else
                obj.rp = 2*obj.rmax;
            end
            fclose(fid);
        end
    end
end

function [z, prof] = zread(fid)
    i = 1;
    z = zeros(2, 1);
    prof = zeros(2, 1);
    while min(z) >= 0
        D = textscan(fid, '%f %f %*s %*s', 1);
        z(i) = D{1};
        prof(i) = D{2};
        i = i+1;
    end
end

function obj = read_slice(obj, Slice)
% Read Slice object and prepare a corresponding AT.RAM_IN object
    nseg_lambda = 15;	% number of segments per wavelength for depth spacing
    dr_factor   = 4;	% depth spacing multiplier for range spacing
    fuzz        = 1.125;	% wiggle room for output spacing
    Pos = Slice.Pos;
    SSP = Slice.SSP;
    bathy_rd = Slice.bathy_rd;
    Bdry = Slice.Bdry;
    
    obj.freq = Slice.freq;
    obj.zs = Pos.s.depth(1); % depth of the source
    obj.zr = Pos.r.depth(1); % depth of the TL line
    lambda = 1500.0/Slice.freq;
    est_dz = lambda/nseg_lambda;
    est_dr = dr_factor*est_dz;
    obj.dz = est_dz;
    obj.dr = est_dr;
    
    % compute range skip factor
    d_range = 1000.0 * (Pos.r.range(2) - Pos.r.range(1));  % convert km -> m
    ndr = round(d_range/obj.dr);
    obj.ndr = max(ndr, 1);
    
    % adjust dr to get consistent spacing of the output
    obj.dr = d_range/obj.ndr;
    
    % sanity check on the adjusted value
    if (obj.dr > fuzz * est_dr)
        warning([ mfilename, ':adjusted value of dr could be too coarse']);
    end
    
    % compute depth skip factor
    d_depth = Pos.r.depth(1);
    ndz = round(d_depth/obj.dz);
    obj.ndz = max(ndz, 1);
    
    % adjust dz to get consistent spacing of the output
    obj.dz = d_depth/obj.ndz;
    
    % sanity check on the adjusted value
    if (obj.dz > fuzz * est_dz)
        warning([ mfilename, ':adjusted value of dz could be too coarse']);
    end
    
    obj.zmax = 1.5*SSP.depth(2);            % maximum depth(m)
    obj.rmax = 1000.0*Pos.r.range(end);     % maximum range(m)    
    obj.zmplt = Pos.r.depth(end);
    obj.c0 = min(SSP.raw(1).alphaR);        % reference sound speed
    obj.np = 8;                             % number of Pade terms
    obj.ns = 1;                             % number of stability constraints
    obj.rs = 0.0;                           % maximum range of stability constraints
    
    % Bathymetry
    if ~isempty(bathy_rd)
        obj.rb = 1000.0 * bathy_rd(:, 1);   % convert from km -> m
        obj.zb =          bathy_rd(:, 2);
    else
        % defaults
        obj.rb = [           0.0;          1.0e9];
        obj.zb = [Bdry.Bot.depth; Bdry.Bot.depth];
    end
    
    % sound speed in water column
    medium = 1;
    obj.zcw = [SSP.raw(medium).z -1];
    obj.dcw = [SSP.raw(medium).alphaR -1];
    
    % sediment properties
    % sound speed
    obj.zcb = [0.0 -1];
    obj.dcb = [Bdry.Bot.HS.alphaR -1];
    
    % density
    obj.zrhob = [0.0 -1];
    obj.drhob = [Bdry.Bot.rho -1];
    
    % attenuation
    obj.zattn = [0.4*SSP.depth(2) 0.5*SSP.depth(2) -1];
    obj.dattn = [Bdry.Bot.HS.alphaI             10 -1];
end
