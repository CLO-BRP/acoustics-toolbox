function  Slice = read_env( Slice, envfil, model )
% Read an environmental file

% should check here that model is one of the known ones

disp( '' )
disp( '' )
disp( '' )
disp( '__________________________________________________________________' );

if ( ~strcmp( envfil, 'ENVFIL' ) && ~strcmp( envfil( end-3: end ), '.env' ) )
  envfil = [ envfil '.env' ]; % append extension
end

[TitleEnv, freq, SSP, Bdry, fid] = AT.read_env_core( envfil );    % read in the environmental file
Slice.TitleEnv = TitleEnv;
Slice.freq = freq;
Slice.SSP = SSP;
Slice.Bdry = Bdry;

if any(strcmpi( model, {'SCOOTER', 'KRAKEN', 'KRAKENC', 'KRAKEL', 'SPARC'}))
    cInt.Low   = fscanf( fid, '%f', 1 );   % lower phase speed limit
    cInt.High  = fscanf( fid, '%f', 1 );   % upper phase speed limit
    fprintf( '\n cLow = %8.1f m/s  cHigh = %8.1f m/s\n', cInt.Low, cInt.High )
    fgetl( fid );
    
    RMax  = fscanf( fid, '%f', 1 );   % read max range, Rmax
    fprintf( 'RMax = %f km \n', RMax )
    fgetl( fid );
    
else   % dummy values for BELLHOP
    Slice.cInt.Low  = 1500;
    Slice.cInt.High = 1e9;
    Slice.RMax      = 100;
end

Pos = AT.readsdrd( fid );                           % read in the source and receiver depths

if ( strcmp( model, 'BELLHOP' ) )
    Pos.r.range = AT.readr( fid );     % read in receiver ranges
    Pos.Nrr     = length( Pos.r.range );
    Beam        = AT.read_bell( fid, Bdry, freq, Bdry.Bot.depth, Bdry.Top.depth, Pos.r.range( end ) );
else   % dummy value for models that don't use Beam parameters
    Beam.RunType    = 'CG';
    Beam.Nbeams     = 0;
    Beam.alpha( 1 ) = -15;
    Beam.alpha( 2 ) = +15;
    Beam.Box.z      = 1.05 * max( Pos.r.depth );
    Beam.Box.r      = 1.05 * RMax;
    Beam.deltas     = 0;
    Pos.r.range     = linspace( 0, RMax, 501 );   % set up receiver range vector
end
Slice.Pos = Pos;
Slice.Beam = Beam;

fclose(fid);

