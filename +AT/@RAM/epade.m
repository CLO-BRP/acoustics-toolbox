function obj = epade(obj, ip)
%EPADE method to calculate coefficients of the rational approximation
    sig = obj.k0*obj.dr;
    n = 2*obj.np;
    if ip == 1
        nu = 0.0;
        alp = 0.0;
    else
        nu = 1.0;
        alp = -0.25;
    end
    % Factorials
    fact = double(cumprod(1:n))';
    % Binomial coefficients
    bin = eye(n);
    bin(:,1) = 1.0;
    for i = 3:n+1
        for j = 2:i-1
            bin(i,j) = bin(i-1,j-1)+bin(i-1,j);
        end
    end
    a = zeros(n);
    % Accuracy constraints
    dg = deriv(n, sig, alp, bin, nu);
    b = dg(2:n+1);
    for i = 1:n
        if(2*i-1) <= n
            a(i,2*i-1) = fact(i);
        end
        for j = 1:i
            if 2*j <= n
                a(i,2*j) = -bin(i+1,j+1)*fact(j)*dg(i-j+1);
            end
        end
    end
    % Stability constraints
    if obj.ns>=1
        z1 = -3.0;
        b(n) = -1.0;
        for j = 1:obj.np
            a(n,2*j-1) = z1^j;
            a(n,2*j) = 0.0;
        end
    end
    if obj.ns>=2
        z1 = -1.5;
        b(n-1) = -1.0;
        for j = 1:obj.np
            a(n-1,2*j-1) = z1^j;
            a(n-1,2*j) = 0.0;
        end
    end
    b = a\b;
    dh1 = zeros(1, obj.np+1);
    dh1(obj.np+1) = 1.0;
    dh1(1:obj.np) = b(2*(obj.np:-1:1)-1);
    dh2 = roots(dh1);
    obj.pd1(1, :) = -1./dh2;
    dh1(1:obj.np) = b(2*(obj.np:-1:1));
    dh2 = roots(dh1);
    obj.pd2(1, :) = -1./dh2;
end

function dg = deriv(n, sig, alp, bin, nu)
%DERIV derivatives of the operator function at x = 0.
    dh1 = zeros(n, 1);
    dh2 = zeros(n, 1);
    dh3 = zeros(n, 1);
    dh1(1) = 0.5*1i*sig;
    exp1 = -0.5 + double(0:-1:-n+2)';
    dh2(1) = alp;
    exp2 = -double(1:n-1)';
    dh3(1) = -2*nu;
    exp3 = -exp2;
    dh1(2:n) = dh1(1)*cumprod(exp1);
    dh2(2:n) = dh2(1)*cumprod(exp2);
    dh3(2:n) = nu*dh3(1)*cumprod(exp3);
    dg(1) = 1;
    dh = dh1 + dh2 + dh3;
    dg(2:n+1,1) = dh;
    for i=2:n
        for j = 1:i-1
            dg(i+1) = dg(i+1) + bin(i,j)*dh(j)*dg(i-j+1);
        end
    end
end