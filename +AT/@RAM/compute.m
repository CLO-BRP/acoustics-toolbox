function [tl_line, tl_grid] = compute(ram)
%COMPUTE evaluates transmission loss using an AT.RAM instance
    if isempty(ram.r)
        error('Computation attempted on an uninitialized AT.RAM object');
    end
    nr = round(ram.rmax/ram.dr);
    if isempty(ram.tl)
        ram.tl = zeros(nr, 2);
        ram = outpt(ram, 1);
    end
    for i = 2:nr
        ram = ram.updat;
        ram.u = ram.solve(ram.np);
        ram.r = ram.r + ram.dr;
        ram = outpt(ram, i);
    end
    tl_line = ram.tl;
    tl_grid = AT.tlgrid(ram);
end