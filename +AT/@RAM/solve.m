function u = solve(obj, np)
%SOLVE is a tridiagonal solver
    u = obj.u;
    iz = obj.iz;
    iz1 = iz+1;
    r1 = obj.r1(2:end,:);
    r2 = obj.r2(iz1, :);
    r3 = [zeros(1, np); obj.r3(:, 1:np)];
    for j = 1:np
        % The right side
        u = obj.s1(:, j).*[0; u(1:end-1)] + obj.s2(:, j).*u + obj.s3(:, j).*[u(2:end); 0];
        % Solve tridiagonal matrix
        u = AT.solver(u, r1(:, j), r2(j), r3(:, j), iz);
    end
end