classdef RAM
    %RAM is the class for doing a Range-dependent Acoustic Model
    %calculation
    %   This is based on version 1.5 of ram.f in the Acoustics Toolbox
    
    properties
        title               % String containing title of calculation
        mr                  % # of bathymetry points
        ib = 1              % bathymetry index
        rb                  % range of bathymetry point (m)
        zb                  % depth of bathymetry point (m)
        dr                  % range step (m)
        r                   % range (m)
        rmax                % maximum range (m)
        rp                  % range of profile update (m)
        ir                  % range index
        dir                 % range interpolation factor
        mdr = 0             % range decimation counter
        ndr                 % range decimation factor for grid (1 = none)
        np                  % # of Pad� terms
        pd1                 % Pad� numerator coefficients
        pd2                 % Pad� denominator coefficients
        ns                  % # of stability constraints (1, 2)
        rs                  % maximum range of stability constraints
        lz                  % # of layers in the grid plot
        zmplt               % maximum depth of output to tl.grid
        nz                  % # of depth points
        nzplt               % # of depth points to plot
        zmax                % maximum depth (m)
        dz                  % depth grid spacing (m)
        ndz                 % depth decimation factor for grid (1 = none)
        iz                  % depth index
        jz                  % another depth index
        eta                 % factor in profile calculation
        freq                % source frequency (Hz)
        zs                  % source depth (m)
        zr                  % receiver depth for tl.line (m)
        omega               % angular freq (rad/s)
        c0                  % reference sound speed (m/s)
        k0                  % reference wavenumber
        ksq 
        zcw                 % depth of sound speed point (m)
        dcw                 % data sound speed in water column (m/s)
        cw                  % sound speed profile in water column (m/s)
        alpw                % relative sound speed in water column
        ksqw                
        zcb                 % depth of sediment point (m)
        dcb                 % data sound speed in sediment (m/s)
        cb                  % sound speed profile in sediment (m/s)
        zrhob               % depth of sediment point (m)
        drhob               % data density of sediment (g/cc)
        rhob                % density profile of sediment (g/cc)
        alpb                % relative sound speed in sediment
        ksqb
        zattn               % depth of sediment point (m)
        dattn               % data attenuation in sediment (dB/wavelength)
        attn                % attenuation profile in sediment (dB/wavelength)
        tl                  % transmission loss result (dB)
        tlg                 % transmission loss grid result 9dB)
        u                   % transmission loss vector (ratio)
        f1
        f2
        f3
        r1
        r2
        r3
        s1
        s2
        s3
        R1
        R2
    end
    
    methods
        function obj = RAM(a)
            if nargin == 0
                obj = AT.RAM(AT.RAM_IN);
                return
            elseif islogical(a)
                if ~a
                    obj = AT.RAM.empty;
                    return
                end
            elseif isa(a, 'AT.RAM') % Copy constructor
                obj = a;
            elseif isa(a, 'AT.RAM_IN')
                obj = copyprops(AT.RAM(true), a);
            elseif ischar(a) % Input is a file
                if ~exist(a, 'file')
                    warning('File %s not found', a);
                    return
                end
                if strfind(a, '.mat')  % matfile
                    in = load(a);
                    names = fieldnames(in);
                    for n = names'
                        if isa(in.(n{:}), 'AT.RAM')
                            obj = in.(n{:});
                            break
                        end
                    end
                    if ~exist('obj', 'variable')
                        for n = names'
                            if isa(in.(n{:}), 'AT.RAM_IN')
                                obj = AT.RAM(in.(n{:}));
                                break
                            end
                        end
                    end
                    if ~exist('obj', 'variable')
                        warning('No AT.RAM or AT.RAM_IN found in %s', a);
                        return
                    end
                elseif strfind(a, '.in')  % ASCII input file
                    obj = AT.RAM(AT.RAM_IN(a));
                    return
                else
                    warning('RAM didn''t find recognized file type in %s -- using default', a);
                    obj = AT.RAM;
                end
            end
            if ~islogical(a)
                obj.eta = 1/(40*pi*log10(exp(1)));
                obj.r = obj.dr;
                obj.omega = 2*pi*obj.freq;
                ri = 1 + obj.zr/obj.dz;
                obj.ir = floor(ri);
                obj.dir = ri - obj.ir;
                obj.k0 = obj.omega/obj.c0;
                obj.nz = floor(obj.zmax/obj.dz - 0.5);
                obj.nzplt = floor(obj.zmplt/obj.dz);
                tiz = floor(1 + obj.zb(1)/obj.dz);
                tiz = max(tiz, 2);
                obj.iz = min(tiz, obj.nz);
                if obj.rs < obj.dr
                    obj.rs = 2*obj.rmax;
                end
                obj.mr = size(obj.rb, 1);
                if isempty(obj.r1)
                    obj.r1 = zeros(obj.nz+2, obj.np);
                    obj.r3 = obj.r1;
                    obj.u = zeros(obj.nz+2, 1);
                end
                obj.lz = floor(obj.nzplt/obj.ndz);
                obj.pd1 = zeros(1, obj.np);
                obj.pd2 = zeros(1, obj.np);
                obj = profl(obj);
                obj = selfs(obj);
                obj = outpt(obj, 1);
                % Propogation matrices
                obj = epade(obj, 1);
                obj = matrc(obj);
            end
        end
        
        function obj = copyprops(obj, a)
            props = properties(a);
            for p = props'
                obj.(p{:}) = a.(p{:});
            end
        end
    end
end
