function ram = updat(ram)
%UPDAT does matrix updates for RAM
    % Varying bathymetry
    if ram.r >= ram.rb(ram.ib+1)
        ram.ib = ram.ib+1;
    end
    ram.jz = ram.iz;
    z = ram.zb(ram.ib) + (ram.r + 0.5*ram.dr - ram.rb(ram.ib))*(ram.zb(ram.ib+1)...
        - ram.zb(ram.ib))/(ram.rb(ram.ib+1) - ram.rb(ram.ib));
    tiz = floor(1.0 + z/ram.dz);
    if tiz ~= ram.iz
        tiz = max(2, tiz);
        ram.iz = min(tiz, ram.nz);
        if ram.iz ~= ram.jz
            ram = ram.matrc;
        end
    end
    % Varying profiles.
    if ram.r >= ram.rp
        ram = ram.profl;
        ram = ram.matrc;
    end
    % Turn off stability constraints.
    if ram.r >= ram.rs
        ram.ns = 0;
        ram.rs = 2.0*ram.rmax;
        ram = ram.epade(1);
        ram = ram.matrc;
    end
end