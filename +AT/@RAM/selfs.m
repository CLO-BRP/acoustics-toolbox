function obj = selfs(obj)
%SELFS is a self-starter method for a RAM object
    % Conditions for the delta function
    si = 1 + obj.zs/obj.dz;
    is = floor(si);
    dis = si - is;
    obj.u(is) = (1 - dis)*sqrt(2*pi/obj.k0)/(obj.dz*obj.alpw(is));
    obj.u(is+1) = dis*sqrt(2*pi/obj.k0)/(obj.dz*obj.alpw(is));
    % Divide the delta function by (1-X)^2 to get a smooth rhs
    obj.pd1(1) = 0.0;
    obj.pd2(1) = -1.0;
    obj.jz = obj.iz;
    obj = obj.matrc;
    obj.u = obj.solve(1);
    obj.u = obj.solve(1);
    % Apply the operator (1-X)^2*(1+X)^(-1/4)*exp(i*k0*r*sqrt(1+X))
    obj = obj.epade(2);
    obj = obj.matrc;
    obj.u = obj.solve(obj.np);
end