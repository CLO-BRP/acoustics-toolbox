function obj = matrc(obj)
%MATRC Calculates triangular matrices
    a1 = obj.k0^2/6;
    a2 = 2*obj.k0^2/3;
    a3 = a1;
    cfact = 0.5/(obj.dz^2);
    dfact = 1.0/12.0;

    % New matrices when obj.iz = obj.jz

    if obj.iz == obj.jz
        i1 = 2;
        i2 = obj.nz+1;
        urange = obj.iz+1:obj.nz+2;
        obj.f1 = 1.0./obj.alpw(1:obj.iz);
        obj.f1(urange) = obj.rhob(urange)./obj.alpb(urange);
        obj.f2(1:obj.iz, 1) = 1.0;
        obj.f2(urange) = 1.0./obj.rhob(urange);
        obj.f3 = obj.alpw(1:obj.iz);
        obj.f3(urange) = obj.alpb(urange);
        obj.ksq = obj.ksqw(1:obj.iz);
        obj.ksq(urange) = obj.ksqb(urange);
    elseif obj.iz > obj.jz
        i1 = obj.jz;
        i2 = obj.iz+1;
        range = obj.jz+1:obj.iz;
        obj.f1(range) = 1.0./obj.alpw(range);
        obj.f2(range) = 1.0;
        obj.f3(range) = obj.alpw(range);
        obj.ksq(range) = obj.ksqw(range);
    elseif obj.iz < obj.jz
        i1 = obj.iz;
        i2 = obj.jz+1;
        range = obj.iz+1:obj.jz;
        obj.f1(range) = obj.rhob(range)./obj.alpb(range);
        obj.f2(range) = 1.0./obj.rhob(range);
        obj.f3(range) = obj.alpb(range);
        obj.ksq(range) = obj.ksqb(range);
    end
    irange = (i1:i2)';
    if (i2-i1) < obj.nz-1  % update
        s1 = zeros(i2, obj.np);
    else  % start over
        s1 = zeros(size(obj.r1));
    end
    s2 = s1;
    s3 = s1;
    
    % Discretization by Galerkin's method
    
    c1(irange, 1) = cfact*obj.f1(irange).*(obj.f2(irange-1)+obj.f2(irange)).*obj.f3(irange-1);
    c2(irange, 1) = -cfact*obj.f1(irange).*(obj.f2(irange-1)+2*obj.f2(irange)+obj.f2(irange+1)).*obj.f3(irange);
    c3(irange, 1) = cfact*obj.f1(irange).*(obj.f2(irange)+obj.f2(irange+1)).*obj.f3(irange+1);
    d1(irange, 1) = c1(irange) + dfact*(obj.ksq(irange-1)+obj.ksq(irange));
    d2(irange, 1) = c2(irange) + dfact*(obj.ksq(irange-1)+6*obj.ksq(irange)+obj.ksq(irange+1));
    d3(irange, 1) = c3(irange) + dfact*(obj.ksq(irange)+obj.ksq(irange+1));
    obj.r1(irange,:) = a1 + d1(irange)*obj.pd2;
    obj.r2(irange,:) = a2 + d2(irange)*obj.pd2;
    obj.r3(irange,:) = a3 + d3(irange)*obj.pd2;
    s1(irange,:) = a1 + d1(irange)*obj.pd1;
    s2(irange,:) = a2 + d2(irange)*obj.pd1;
    s3(irange,:) = a3 + d3(irange)*obj.pd1;
    obj.r1(obj.nz+2, :) = zeros(1, obj.np);
    
    % matrix decomposition
    
    for i = i1:obj.iz
        rfact = 1.0./(obj.r2(i,:) - obj.r1(i,:).*obj.r3(i-1,:));
        obj.r1(i,:) = obj.r1(i,:).*rfact;
        obj.r3(i,:) = obj.r3(i,:).*rfact;
        s1(i,:) = s1(i,:).*rfact;
        s2(i,:) = s2(i,:).*rfact;
        s3(i,:) = s3(i,:).*rfact;
    end   
    for i = i2:-1:obj.iz+2
        rfact = 1.0./(obj.r2(i,:) - obj.r3(i,:).*obj.r1(i+1,:));
        obj.r1(i,:) = obj.r1(i,:).*rfact;
        obj.r3(i,:) = obj.r3(i,:).*rfact;
        s1(i,:) = s1(i,:).*rfact;
        s2(i,:) = s2(i,:).*rfact;
        s3(i,:) = s3(i,:).*rfact;
    end
    iz1 = obj.iz+1;
    obj.r2(iz1,:) = obj.r2(iz1,:) - obj.r1(iz1,:).*obj.r3(iz1-1,:) ...
        - obj.r3(iz1,:).*obj.r1(iz1+1,:);
    obj.r2(iz1,:) = 1./obj.r2(iz1,:);
    if (i2-i1) < obj.nz-1 % update arrays
        obj.s1(irange, :) = s1(irange, :);
        obj.s2(irange, :) = s2(irange, :);
        obj.s3(irange, :) = s3(irange, :);
    else
        obj.s1 = s1;
        obj.s2 = s2;
        obj.s3 = s3;
    end
end
