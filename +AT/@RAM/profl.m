function obj = profl(obj)
% PROFL is a method in the AT.RAM class that calculates profile data
    obj.cw = interpolate(obj.dcw, obj.zcw, obj.dz, obj.nz);
    obj.cb = interpolate(obj.dcb, obj.zcb, obj.dz, obj.nz);
    obj.rhob = interpolate(obj.drhob, obj.zrhob, obj.dz, obj.nz);
    obj.attn = interpolate(obj.dattn, obj.zattn, obj.dz, obj.nz);
    obj.ksqw = (obj.omega./obj.cw).^2 - obj.k0*obj.k0;
    obj.ksqb = ((obj.omega./obj.cb).*(1 + 1i*obj.eta*obj.attn)).^2 - obj.k0*obj.k0;
    obj.alpw = sqrt(obj.cw./obj.c0);
    obj.alpb = sqrt(obj.rhob.*obj.cb/obj.c0);
end

function prof = interpolate(dprof, zprof, dz, nz)
% interpolate utility function that does piecewise interpolation of data
    if dprof(end) < 0
        dprof(end) = dprof(end-1);
    end
    xprof = floor(1.5 + zprof/dz);
    if zprof(end) < 0
        xprof(end) = nz + 2;
    end
    [xprof, IX] = sort(xprof);
    dprof = dprof(IX);
    xprof(xprof>(nz+2)) = [];
    for i = 1:length(xprof)-1
        interval = xprof(i+1) - xprof(i);
        if interval > 0
            prof(xprof(i):xprof(i+1)) = dprof(i) + (0:interval)*(dprof(i+1) - dprof(i))/...
                interval;
        end
    end
    prof = prof';
end