function ram = outpt(ram, i)
%OUTPT method to save results from an update of a RAM object
    if i == 1
        nr = round(ram.rmax/(ram.dr*ram.ndr));
        ram.tl = zeros(nr, 2);
        ram.tlg = zeros(ram.lz, nr);
    end
    if mod(i, ram.ndr) > 0
        return
    end
    ix = floor(i/ram.ndr);
    % attenuation on line
    ur = (1.0 - ram.dir)*ram.f3(ram.ir)*ram.u(ram.ir) + ...
               ram.dir*ram.f3(ram.ir+1)*ram.u(ram.ir+1);
    ram.tl(ix, :) = [ram.r dbcalc(ur, ram.r)];
    % attenuation grid
    iy = (ram.ndz:ram.ndz:ram.nzplt);
    ur = ram.u(iy).*ram.f3(iy);
    ram.tlg(:, ix) = dbcalc(ur, ram.r);
end

function db = dbcalc(ur, r)
%DBCALC converts a vector of signal levels to DB
    eps = 1.e-20;
    db = -20.0*log10(abs(ur)+eps)+10*log10(r+eps);
end