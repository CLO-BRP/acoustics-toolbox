classdef tlgrid
    %tlgrid is the class to contain a transmission loss grid
    
    properties
        title               % string containing title for this instance
        freq                % source frequency (Hz)
        zs                  % source depth (m)
        zr                  % receiver depth for tl.line (m)
        rmax                % maximum range (m)
        dr                  % range step (m)
        ndr                 % range decimation factor for grid (1 = none)
        zmax                % maximum depth (m)
        dz                  % depth grid spacing (m)
        ndz                 % depth decimation factor for grid (1 = none)
        zmplt               % maximum depth of output to tl.grid
        c0                  % reference sound speed (m/s)
        np                  % # of Pad� terms
        ns                  % # of stability constraints (1, 2)
        rs                  % maximum range of stability constraints
        lz                  % # of layers in the grid plot
        r                   % ranges at which tlgrid was calculated
        tlg                 % tl data
    end
    
    methods
        function obj = tlgrid(a)
            if nargin == 0
                obj = AT.tlgrid.empty;
                return
            end
            if isa(a, 'AT.tlgrid') % Copy constructor
                obj = a;
            elseif isa(a, 'AT.RAM')  % usual constructor
                obj.title = a.title;
                obj.freq = a.freq;
                obj.zs = a.zs;
                obj.zr = a.zr;
                obj.rmax = a.rmax;
                obj.dr = a.dr;
                obj.ndr = a.ndr;
                obj.zmax = a.zmax;
                obj.dz = a.dz;
                obj.ndz = a.ndz;
                obj.zmplt = a.zmplt;
                obj.c0 = a.c0;
                obj.np = a.np;
                obj.ns = a.ns;
                obj.rs = a.rs;
                obj.lz = a.lz;
                obj.r = a.tl(:,1);
                obj.tlg = a.tlg;
            else
                warning('Unrecognized input to AT.tlgrid constructor');
            end
        end
        
        function plot(obj)
            pcolor(obj.r, (1:obj.lz)*obj.dz*obj.ndz, obj.tlg);
            set(gca, 'YDir', 'Reverse')
            shading flat;
            colormap(flipud(jet));
            colorbar
            caxis([40 90])
        end
    end
end
