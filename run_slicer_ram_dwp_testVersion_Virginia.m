% RUN_SLICER_RAM: Script to run MATLAB RAM on same model as the original.

% read the coordinate data exported from DR tool
num = xlsread('K:\Software\AcousticEcologyToolbox\DetectionRangeCalc\RAM_Model\Maryland_DRC_Basic-v-RAM_250km_120deg_ch8-11_v3.xlsx','Coord.');

% source level
sl = 189;

% source coordinate
slat = num(2,3);
slon = num(2,2);

% receiver coordinates
rlat = num(3:14,3);
rlon = num(3:14,2);

% range (km)
rng = num(3:14,1);

% Set up Params struct
Params.BathyFile = 'K:\Projects\2013_UnivMD_Maryland_71485\EnvironmentalData\MarylandBathy_v2_xyz.mat';
% removed the environmental file
% Params.EnvFile = 'Stellwagen.env';
Params.EnvFile = [];
% added
Params.SSP_month = 4;

% color me ram
Params.acoustic_model = 'RAM';

% slice me
Params.RunType = 'ENDPOINTS';

% depth of a vocalizing whale (m)?
Params.src_depth = 5;

% make it 20 Hz
Params.src_freq = 20;

% narrow band
Params.src_bw = 0;

%  set parameters
Params.Bot.alphaR = 1593;
Params.Bot.betaR = 0;
Params.Bot.rho = 1.2200;
Params.Bot.alphaI = 0.592;
Params.Bot.betaI = 0;
Params.range_resolution_km = rng(1)/100;
Params.grid_depths = [0 300 200];
Params.src_lat = slat;
Params.src_lon = slon;

hf1 = figure(1);

hf1.Position = getFigureDimensions(85,1);

for ix = 1:length(rng)
    
    tic
    
    Params.rcv_lat = rlat(ix);
    Params.rcv_lon = rlon(ix);
    
    %Instantiate the tl_grid
    tl_grid = HLS.tl_grid(Params);
    TLmat = tl_grid.compute;
    
% % %     profile on
    
    tsec(ix) = toc;
    
    display(['Range ',num2str(rng(ix)),'km =',num2str(tsec(ix)),' secs'])
    
% % %     profile off
    
    % plot TL
    
    rcv_ranges = tl_grid.Pos.r.range;
    rcv_depths = tl_grid.Pos.r.depth;
    rl = sl-TLmat;
    
    subplot(4,4,ix)
    
    imagesc( rcv_ranges, rcv_depths,  rl)
    title( ['RAM > Virginia - Coherent TL @ ',num2str(Params.src_freq),'Hz']);
    xlabel( 'Range (km)' )
    ylabel( 'Depth (m)' )
    
    tej = jet( 256 );  % 'jet' colormap reversed
    colormap( tej )
    
    hcb = colorbar;
    AT.caxisrev( [ prctile(rl(:),1) prctile(rl(:),99) ] )
    
end

subplot(4,4,length(rng)+1)
plot(rng(1:length(rng)),tsec,'-ok');
xlim([0,rng(length(rng))])

title( ['RAM > Virginia - Coherent TL @ ',num2str(Params.src_freq),'Hz']);
xlabel( 'Range (km)' )
ylabel( 'Processing Time (Secs)' )
grid on

printWYSIWYGpng('N:\users\ponirakis_dimitri_dwp22\for_JohnZ\Virginia_Ram_Runs','png',301)