function pathdef
% PATHDEF adds the Acoustics Toolbox to the MATLAB path if it isn't
% there already
    thisdir = pwd;
    if isempty(strfind(path, thisdir))
        addpath(thisdir);
    end
end