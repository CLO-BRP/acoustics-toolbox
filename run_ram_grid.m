% RUN_RAM_GRID: Script to run MATLAB RAM using an HLS.tl_grid

% Set up Params struct
Params.BathyFile = 'Stellwagenxyz.mat';     % NGDC bathymetry for Stellwagen Bank
Params.acoustic_model = 'RAM';              % either 'BELLHOP' or 'RAM'
Params.RunType = 'GRID';                    % 'GRID', 'ENDPOINTS', or 'POLAR'
Params.src_depth = 15.0;                    % source depth in meters
Params.src_freq  = 100.0;                   % source frequency in Hz 
Params.src_bw    = 0.075;                   % percent bandwidth, [max(f)-min(f)]/mean(f)
Params.SSP_month = 6;                       % Month of year for WOA SSP
Params.Bot.alphaR = 1593.0;
Params.Bot.betaR  = 0.0;
Params.Bot.rho    = 1.339;
Params.Bot.alphaI = 0.592;
Params.Bot.betaI  = 0.0;
Params.grid_lat   = [ 42.5,  42.8, 251 ];
Params.grid_lon   = [-70.8, -70.2, 201 ];
Params.grid_depths = [ 10, 50, 5];          % depths: 10, 20, 30, 40, 50 m
Params.bearing_resolution = 2.0;            % resolution of polar angles (degrees)
Params.range_resolution_km = 0.05;          % resolution of range receivers (km)
Params.src_lat    =  42.5;                  % source latitude  in decimal degrees
Params.src_lon    = -70.6;                  % source longitude in decimal degrees

%Instantiate the tl_grid
tl_grid = HLS.tl_grid(Params);
TLmat = tl_grid.compute;

% Example plot
lat_deg = linspace(Params.grid_lat(1), Params.grid_lat(2), Params.grid_lat(3));
lon_deg = linspace(Params.grid_lon(1), Params.grid_lon(2), Params.grid_lon(3));

figure
imagesc(lon_deg, lat_deg, TLmat(:, :, 2));
axis xy
title('Stellwagen Bank Example - Receiver depth = 20 m');
xlabel('Longitude (degrees)');
ylabel('Latitude (degrees)');

tej = flipud(jet(256));  % 'jet' colormap reversed
colormap(tej)
AT.caxisrev([50 110])
