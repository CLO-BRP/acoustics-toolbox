% RUN_SLICER_RAM: Script to run MATLAB RAM on same model as the original.

% read the coordinate data exported from DR tool
num = xlsread('K:\Software\AcousticEcologyToolbox\DetectionRangeCalc\RAM_Model\Maryland_DRC_Basic-v-RAM_250km_120deg_ch8-11_v3.xlsx','Coord.');

% source level
sl = 189;

% source coordinate
slat = num(2,3);
slon = num(2,2);

% receiver coordinates
rlat = num(3:14,3);
rlon = num(3:14,2);

% range (km)
rng = num(3:14,1);

% Set up Params struct
Params.BathyFile = 'K:\Projects\2013_UnivMD_Maryland_71485\EnvironmentalData\MarylandBathy_v2_xyz.mat';
% removed the environmental file
% Params.EnvFile = 'Stellwagen.env';
Params.EnvFile = [];
% added
Params.SSP_month = 4;

% color me ram
Params.acoustic_model = 'RAM';

% slice me
Params.RunType = 'ENDPOINTS';

% depth of a vocalizing whale (m)?
Params.src_depth = 5;

% narrow band
Params.src_bw = 0;

%  set parameters
Params.Bot.alphaR = 1593;
Params.Bot.betaR = 0;
Params.Bot.rho = 1.2200;
Params.Bot.alphaI = 0.592;
Params.Bot.betaI = 0;
Params.range_resolution_km = rng(1)/100;
Params.grid_depths = [0 300 200];
Params.src_lat = slat;
Params.src_lon = slon;

ixr = nearest2(rng,8);

% double up frequencies from 20Hz
fq = 2.^(1:5)*10;

tsec = ones(length(fq),ixr)*NaN;

pmap = distinguishable_colors(length(fq));

for ixf = 1:length(fq)
    
    %     create figure for each frequency
    hf(ixf) = figure(ixf);
    hf(ixf).Position = getFigureDimensions(85,1);
    
    % make it 20 Hz
    Params.src_freq = fq(ixf);
    
    for ix = 1:ixr
        
        tic
        
        Params.rcv_lat = rlat(ix);
        Params.rcv_lon = rlon(ix);
        
        %Instantiate the tl_grid
        tl_grid = HLS.tl_grid(Params);
        TLmat = tl_grid.compute;
        
        % % %     profile on
        
        tsec(ixf,ix) = toc;
        
        display(['Freq. = ',num2str(fq(ixf)),'Hz / Range ',num2str(rng(ix)),'km =',num2str(tsec(ixf,ix)),' secs'])
        
        % % %     profile off
        
        % plot TL
        
        rcv_ranges = tl_grid.Pos.r.range;
        rcv_depths = tl_grid.Pos.r.depth;
        rl = sl-TLmat;
        
        subplot(3,3,ix)
        
        imagesc( rcv_ranges, rcv_depths,  rl)
        title( ['RAM > Virginia - Coherent TL @ ',num2str(Params.src_freq),'Hz']);
        xlabel( 'Range (km)' )
        ylabel( 'Depth (m)' )
        
        tej = jet( 256 );  % 'jet' colormap reversed
        colormap( tej )
        
        hcb = colorbar;
        AT.caxisrev( [ prctile(rl(:),1) prctile(rl(:),99) ] )
        
    end
    
    subplot(3,3,ixr+1)
    hp(ixf) = plot(rng(1:ixr),tsec(ixf,:),'-ok');
    hp(ixf).Color = pmap(ixf,:);
    
    xlim([0,rng(ixr)])
    
    title( ['RAM > Virginia - Coherent TL @ ',num2str(Params.src_freq),'Hz']);
    xlabel( 'Range (km)' )
    ylabel( 'Processing Time (Secs)' )
    grid on
    
    printWYSIWYGpng(['N:\users\ponirakis_dimitri_dwp22\for_JohnZ\Virginia_Ram_Runs_',num2str(fq(ixf)),'Hz'],'png',301)
    
end

figure

hold on

for ixf = 1:length(fq)
    
    hp(ixf) = plot(rng(1:ixr),tsec(ixf,:),'-ok');
    hp(ixf).Color = pmap(ixf,:);
    
end

title( ['RAM > Virginia - Coherent TL @ ',num2str(fq),' Hz']);
xlabel( 'Range (km)' )
ylabel( 'Processing Time (Secs)' )
grid on

leg = cellstr([num2str(fq'),ones(length(fq),1)*' Hz']);
hleg = legend(leg);

xlim([0,rng(ixr)])



printWYSIWYGpng(['N:\users\ponirakis_dimitri_dwp22\for_JohnZ\Virginia_Ram_Runs_v3'],'png',301)
