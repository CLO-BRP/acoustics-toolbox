function Bathy = LoadBathymetry( xyzfile )

% Loads the bathymetry file
%
% assumed to be a rectangular grid of data written in the standard GEODAS
% xyz form
%
% xyzfile should include the extension
%
% Returns a structure:
% Bathy.depth depths
% Bathy.Lat   latitudes
% Bathy.Lon   longitudes
% Bathy.Latkm mapping of lat to km assuming (x,y)=(0,0) corresponds to ( Lon(1), Lat(1) )
% Bathy.Lonkm

Bathy.fname = xyzfile;
[~, ~, ext] = fileparts(xyzfile);
if strcmpi(ext, '.mat')
    load(xyzfile);
else
    xyz = load( xyzfile );
end

Lat  = unique( xyz( :, 2 ) ); % get unique latitudes
Lon  = unique( xyz( :, 1 ) ); % get unique longitudes
Lat  = fliplr( Lat );

nlon = length( Lon );
nlat = length( Lat );

z = reshape( xyz( :, 3 ), nlon, nlat );
z = flipud( z' );

% z( z > 0 ) = NaN;   % remove land

Bathy.depth = -z;
Bathy.Lat   = Lat;
Bathy.Lon   = Lon;

% Create grid in km
LatTotkm = AT.dist_wh( [ Lat( 1 ) Lat( end ) ] , [ Lon( 1 ) Lon( 1   ) ] );
LonTotkm = AT.dist_wh( [ Lat( 1 ) Lat( 1   ) ] , [ Lon( 1 ) Lon( end ) ] );

% mapping of lat/long to km
Bathy.Latkm = linspace( 0, LatTotkm, length( Lat ) ) / 1000;
Bathy.Lonkm = linspace( 0, LonTotkm, length( Lon ) ) / 1000;
