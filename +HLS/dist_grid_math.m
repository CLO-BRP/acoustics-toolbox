function [ A12, range, A21 ] = dist_grid_math( src_lat, src_long, lat, long, argu1 )
%
% this is a version of dist_wh modified to return bearings in the math
% convention, rather than the compass convention (mbp)
% further modified to compute ranges and bearings from a source point to a
% grid defined by lat and long vectors.  Output is matrices of dimension
% nlat x nlon.  (jaz)
%
% DIST_GRID_MATH    Computes distance and bearing between points on the earth
%         using various reference spheroids.
%
%         [AF, RANGE, AR] = DIST_GRID_MATH(SRC_LAT, SRC_LONG, LAT, LONG) computes 
%         the ranges RANGE between a source point and points in a grid
%         specified by the LAT and LONG vectors (decimal degrees with
%         positive indicating north/east). Forward and reverse bearings 
%         (degrees) are returned in AF, AR.
%
%         [..] = DIST_GRID_MATH(...,'ellipsoid') uses the specified ellipsoid
%         to get distances and bearings. Available ellipsoids are:
%
%         'clarke66'  Clarke 1866
%         'iau73'     IAU 1973B
%         'wgs84'     WGS 1984
%         'sphere'    Sphere of radius 6371.0 km
%
%          The default is 'wgs84'.
%
%          Ellipsoid formulas are recommended for distance d<2000 km,
%          but can be used for longer distances.

%Notes: RP (WHOI) 3/Dec/91
%         Mostly copied from BDC "dist.f" routine (copied from ....?), but
%         then wildly modified to bring it in line with Matlab vectorization.
%
%       RP (WHOI) 6/Dec/91
%         Feeping Creaturism! - added geodesic computations. This turned
%         out to be pretty hairy since there were a lot of branch problems
%         with asin, atan when computing geodesics subtending > 90 degrees
%         that were ignored in the original code!
%       RP (WHOI) 15/Jan/91
%         Fixed some bothersome special cases, like when computing geodesics
%         and N=2, or LAT=0...

%C GIVEN THE LATITUDES AND LONGITUDES (IN DEG.) IT ASSUMES THE IAU SPHERO
%C DEFINED IN THE NOTES ON PAGE 523 OF THE EXPLANATORY SUPPLEMENT TO THE
%C AMERICAN EPHEMERIS.
%C
%C THIS PROGRAM COMPUTES THE DISTANCE ALONG THE NORMAL
%C SECTION (IN M.) OF A SPECIFIED REFERENCE SPHEROID GIVEN
%C THE GEODETIC LATITUDES AND LONGITUDES OF THE END POINTS
%C  *** IN DECIMAL DEGREES ***
%C
%C  IT USES ROBBIN'S FORMULA, AS GIVEN BY BOMFORD, GEODESY,
%C FOURTH EDITION, P. 122.  CORRECT TO ONE PART IN 10**8
%C AT 1600 KM.  ERRORS OF 20 M AT 5000 KM.
%C
%C   CHECK:  SMITHSONIAN METEOROLOGICAL TABLES, PP. 483 AND 484,
%C GIVES LENGTHS OF ONE DEGREE OF LATITUDE AND LONGITUDE
%C AS A FUNCTION OF LATITUDE. (SO DOES THE EPHEMERIS ABOVE)
%C
%C PETER WORCESTER, AS TOLD TO BRUCE CORNUELLE...1983 MAY 27
%C
spheroid='wgs84'; 
if (nargin >= 5),
   if (ischar(argu1)),
      spheroid=argu1;
   else
      error('The fifth argument must be the name of a spheroid');
   end;
end;

switch spheroid(1:3)
    case 'sph'
        A = 6371000.0;
        B = A;
        E = 0.0;
        EPS = 0.0;
    case 'cla' 
        A = 6378206.4E0;
        B = 6356583.8E0;
        E = sqrt(A*A-B*B)/A;
        EPS = E*E/(1.-E*E);
    case 'iau'
        A = 6378160.e0;
        B = 6356774.516E0;
        E = sqrt(A*A-B*B)/A; 
        EPS = E*E/(1.-E*E);
    case 'wgs'

%c on 9/11/88, Peter Worcester gave me the constants for the 
%c WGS84 spheroid, and he gave A (semi-major axis), F = (A-B)/A
%c (flattening) (where B is the semi-minor axis), and E is the
%c eccentricity, E = ( (A**2 - B**2)**.5 )/ A
%c the numbers from peter are: A=6378137.; 1/F = 298.257223563
%c E = 0.081819191
        A = 6378137.;
        E = 0.081819191;
        EPS = E*E/(1.-E*E);
        B = A*sqrt(1.-E*E);
    otherwise
        error('dist_grid_math: Unknown spheroid specified!');
end

if ~isscalar(src_lat) || ~isscalar(src_long)
    error('dist_grid_math: The source must have a unique latitude and longitude');
end

lat = lat(:)*pi/180;     % convert to radians
long = long(:)*pi/180;
src_lat = src_lat*pi/180;
src_long = src_long*pi/180;

PHI1 = src_lat;    % endpoints of each segment
XLAM1 = src_long;
[XLAM2, PHI2] = meshgrid(long, lat);

                 % wiggle lines of constant lat to prevent numerical probs.
if (any(src_lat==lat))
   PHI1 = PHI1 + 1e-14;
end
                 % wiggle lines of constant long to prevent numerical probs.
if (any(src_long==long)), 
   XLAM1 = XLAM1 + 1e-14;
end

%C  COMPUTE THE RADIUS OF CURVATURE IN THE PRIME VERTICAL FOR
%C EACH POINT

xnu1 = A/sqrt(1.0 - (E*sin(src_lat))^2);
xnu2 = A./sqrt(1.0 - (E*sin(PHI2)).^2);

%C*** COMPUTE THE AZIMUTHS.  A12 (A21) IS THE AZIMUTH AT POINT 1 (2)
%C OF THE NORMAL SECTION CONTAINING THE POINT 2 (1)

TPSI2 = (1-E*E)*tan(PHI2) + E*E*xnu1*sin(PHI1)./(xnu2.*cos(PHI2));
PSI2 = atan(TPSI2);

%C*** SOME FORM OF ANGLE DIFFERENCE COMPUTED HERE??

DPHI2 = PHI2-PSI2;
DLAM = XLAM2-XLAM1;
CTA12 = (cos(PHI1)*TPSI2 - sin(PHI1)*cos(DLAM))./sin(DLAM);
A12 = atan(1./CTA12);

%C    GET THE QUADRANT RIGHT
DLAM2 = (abs(DLAM)<pi).*DLAM + (DLAM>=pi).*(-2*pi+DLAM) + ...
        (DLAM<=-pi).*(2*pi+DLAM);
A12 = A12+(A12<-pi)*2*pi-(A12>=pi)*2*pi;
A12r = A12+pi*sign(-A12).*( sign(A12) ~= sign(DLAM2) );
A12 = A12r*180/pi;

% here's the trivial mod to return bearings in the math convention
A12 = mod( -A12 + 90, 360 );

if nargout == 1
    return
end

SSIG = sin(DLAM).*cos(PSI2)./sin(A12r);
% At this point we are OK if the angle < 90...but otherwise
% we get the wrong branch of asin! 
% This fudge will correct every case on a sphere, and *almost*
% every case on an ellipsoid (wrong handling will be when
% angle is almost exactly 90 degrees)
dd2 = cat(3, cos(XLAM2).*cos(PHI2), sin(XLAM2).*cos(PHI2), sin(PHI2));
dd1 = cat(3, cos(XLAM1)*cos(PHI1), sin(XLAM1)*cos(PHI1), sin(PHI1));
dd2 = bsxfun(@minus, dd2, dd1);
dd2 = sum(dd2.*dd2, 3);
if any(abs(dd2-2) < 2*((B-A)/A))
    disp('dist_grid_math: Warning...point(s) too close to 90 degrees apart');
end
bigbrnch = dd2>2;
 
SIG=asin(SSIG).*(bigbrnch==0) + (pi-asin(SSIG)).*bigbrnch;
     
%C   COMPUTE RANGE

G2 = EPS*(sin(PHI1)).^2;
G = sqrt(G2);
H2 = EPS*(cos(PHI1).*cos(A12r)).^2;
H = sqrt(H2);
TERM1 = -SIG.*SIG.*H2.*(1.0-H2)/6.0;
TERM2 = (SIG.^3).*G.*H.*(1.0-2.0*H2)/8.0;
TERM3 = (SIG.^4).*(H2.*(4.0-7.0*H2)-3.0*G2.*(1.0-7.0*H2))/120.0;
TERM4 = -(SIG.^5).*G.*H/48.0;

range = xnu1*SIG.*(1.0+TERM1+TERM2+TERM3+TERM4);

if nargout == 2
    return
end

CTA21P = (sin(PSI2).*cos(DLAM) - cos(PSI2).*tan(PHI1))./sin(DLAM);
A21P = atan(1/CTA21P);
A21P = A21P+(A21P<-pi)*2*pi-(A21P>=pi)*2*pi;
A21P = A21P+pi*sign(-A21P).*( sign(A21P) ~= sign(-DLAM2) );
A21 = A21P - DPHI2.*sin(A21P).*tan(SIG/2.0);
A12 = A12*180/pi;

% here's the trivial mod to return bearings in the math convention
A21 = mod( -A21 + 90, 360 );
