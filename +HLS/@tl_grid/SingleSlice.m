function [rv, rc] = SingleSlice(Slice)
%COMPUTE_SLICE -
% setup the information needed for Arrivals, or TL calculation and
% run the selected acoustics model for the given slice (vertical plane)
%
%    rv is the returned value (usually TL)
%    rc is a completion code (0 for success)
% 
% The primary tasks are to extract the bathymetry and range-dependent
% sound speed profile for the specified vertical plane.
%
% There are two methods available to specify the vertical plane. Provide:
%  1) lat/lon of the source and the lat/lon of most distant receiver
%  2) lat/lon of the source and the bearing and range scalar values

    bty_file        = Slice.Bdry.Bot.Opt( 2 );

    % If requested, interpolate the bathymetry along the computational plane

    if bty_file == '*'
        Plane           = Slice.Plane;
        bathy_delta_km  = Slice.bathy_delta_km;
        max_r_km        = Plane.max_r_km;
        del_r           = max_r_km / Plane.nrr;
        rr              = Slice.Pos.r.range;
        ssp_depth       = Slice.SSP.depth(2);

       % Determine range limit for bathymetry interpolation

       if Slice.nrr > 1
          bathy_range_km = rr( end ) + del_r;
       else
          bathy_range_km = rr( 1   ) + 0.5;
       end

       % Interpolate the bathymetry for the specified computational plane

       Interp.src_lat        = Plane.src_lat;
       Interp.src_lon        = Plane.src_lon;
       Interp.bearing_deg    = Plane.bearing_deg;
       Interp.bathy_range_km = bathy_range_km;
       Interp.bathy_delta_km = bathy_delta_km;

       rd = HLS.bathy_interp( Slice.BathyData, Interp );

       Slice.bathy_rd = rd;

       % Verify the SSP extends to the deepest point in the interpolated bathymetry

       max_depth = max( rd( :, 2 ) );

       if max_depth > ssp_depth
          error_msg = sprintf( ': max depth of sound speed profile (%.2f) does not extend to bottom (%.2f)', ...
             ssp_depth, max_depth );
          error( [ mfilename, error_msg ] );
       end

       % Reset the limits of the bounding box for ray tracing models (BELLHOP)

       Slice.Beam.Box.r = bathy_range_km;
       Slice.Beam.Box.z = max_depth + 1;
    end

    %%

    % run the acoustic model

    acoustic_model = upper( Slice.acoustic_model );

    switch acoustic_model
       case 'BELLHOP'
          Slice.write_bellhop_files(Slice.SlicerParams);  % write the BELLHOP input files
          [ rv, rc ] = Slice.run_bellhop(Slice.SlicerParams);
          
       case 'BELLHOP-MEX'
          [ rs, rc] = Slice.bellhop;
          Ntheta = sscanf(num2hex(rs(2, 3)), '%x');
          Nsd = sscanf(num2hex(rs(5, 3)), '%x');
          Nrd = sscanf(num2hex(rs(6, 3)), '%x');
          Nrr = sscanf(num2hex(rs(7, 3)), '%x');
          p = zeros(Ntheta, Nsd, Nrd, Nrr);
          for itheta = 1 : Ntheta
             for isd = 1 : Nsd
                 for ird = 1 : Nrd
                    recnum = 9 + (itheta-1) * Nsd * Nrd + (isd-1) * Nrd + ird;
                    p(itheta, isd, ird, :) = rs(1:2:2*Nrr, recnum) + 1i*rs(2:2:2*Nrr, recnum);
                 end
             end
          end
          tl = abs( squeeze( p( 1, 1, :, : ) ) );
          tl( isnan( tl ) ) = 1.0d-6;
          tl( isinf( tl ) ) = 1.0d-6;
          tl( tl < 1.0d-6 ) = 1.0d-6;
          rv = -20.0 * log10( tl );		% transmission loss in dB
          
       case 'BELLHOP-MATLAB'
          [ rv, rc ] = Slice.bellhopM;
          
       case 'RAM'
          RAM_IN_from_Slice = AT.RAM_IN(Slice);
          ram = AT.RAM(RAM_IN_from_Slice);  % create a RAM object
          [ ~, rv ] = ram.compute;
          rc = 0;

       otherwise
          error( [ mfilename, ': error: unknown or unsupported acoustic model: ', acoustic_model ] );
    end

end

