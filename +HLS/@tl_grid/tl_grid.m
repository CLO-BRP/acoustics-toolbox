classdef tl_grid < AT.Slice
    %tl_grid - object for computing transmission loss on a rectangular grid 
    % (in lat/lon)
    %   This class derives from Slice and requires a Params structure for
    %   its constructor
    
    properties
        alpha         = 0.0
        bearings
        fullcircle
        grid_bearing_deg
        grid_range_km
        nbearings
        no_pts
        nrd
        nrr
        range_divisor
        range_mask
        BathyData
        SlicerParams
    end

    methods
        function tlgd = tl_grid(Params)  % constructor
            if nargin == 0 || ~isstruct(Params)
                error('HLS.tl_grid requires a parameter structure for its constructor');
            end

            if strcmp(Params.acoustic_model, 'BELLHOP')
                tlgd.SlicerParams = HLS.slicer_init;
            end
            tlgd.BathyData = HLS.LoadBathymetry(Params.BathyFile);
            
            src_lat = Params.src_lat;
            src_lon = Params.src_lon;
            
            if isfield(Params, 'EnvFile') && ~isempty(Params.EnvFile)
                Slice = AT.Slice(Params.EnvFile);
                tlgd = copyprops(tlgd, Slice);
            else
                % List of the WOA monthly files
                WOA_files = { '01-January.mat', ...
                              '02-February.mat', ...
                              '03-March.mat', ...
                              '04-April.mat', ...
                              '05-May.mat', ...
                              '06-June.mat', ...
                              '07-July.mat', ...
                              '08-August.mat', ...
                              '09-September.mat', ...
                              '10-October.mat', ...
                              '11-November.mat', ...
                              '12-December.mat' };
                SSPdata = load( [fileparts(mfilename('fullpath')) filesep 'WOA' filesep WOA_files{Params.SSP_month}] );
                
                Ndepths = length( SSPdata.Oceanography.depth );
                [~, i_lat] = min( abs( SSPdata.Oceanography.lat - src_lat ) );
                [~, i_lon] = min( abs( SSPdata.Oceanography.lon - src_lon ) );
                
                tlgd.SSP.depth(2)   = SSPdata.Oceanography.depth( end );
                tlgd.SSP.raw.z      = SSPdata.Oceanography.depth( : )';
                tlgd.SSP.raw.alphaR = squeeze( SSPdata.Oceanography.c( i_lon, i_lat, : ) )';
                tlgd.SSP.raw.betaR  = zeros( 1, Ndepths );
                tlgd.SSP.raw.rho    =  ones( 1, Ndepths );
                tlgd.SSP.raw.alphaI = zeros( 1, Ndepths );
                tlgd.SSP.raw.betaI  = zeros( 1, Ndepths );
            end

            % Populate the tl_grid parameters with the user's values

            tlgd.acoustic_model = Params.acoustic_model;

            tlgd.freq = Params.src_freq;

            tlgd.Beam.RunType( 1 ) = 'C';

            tlgd.Bdry.Bot.alphaR = Params.Bot.alphaR;
            tlgd.Bdry.Bot.betaR  = Params.Bot.betaR;
            tlgd.Bdry.Bot.rho    = Params.Bot.rho;
            tlgd.Bdry.Bot.alphaI = Params.Bot.alphaI;
            tlgd.Bdry.Bot.betaI  = Params.Bot.betaI;

            tlgd.Pos.s.depth = Params.src_depth;


            switch Params.RunType
                case 'GRID'
                    % Setup the lat, lon grid where TL values will be computed

                    grid_lat = Params.grid_lat;
                    grid_lon = Params.grid_lon;

                    nlat = grid_lat( 3 );
                    nlon = grid_lon( 3 );

                    lat = linspace( grid_lat( 1 ), grid_lat( 2 ), nlat );
                    lon = linspace( grid_lon( 1 ), grid_lon( 2 ), nlon );
                    %%

                    % Find a suitable branch cut for the bearing angles of the rectangular box
                    % for the receiver grid
                    branch = [ 0 360 ];   % default branch is a full sweep

                    % get bearings to the 4 corners of the receiver box
                    box_bearings = HLS.dist_grid_math( src_lat, src_lon, grid_lat(1:2), grid_lon(1:2) );
                    bearing1 = box_bearings(1, 1);
                    bearing2 = box_bearings(1, 2);
                    bearing3 = box_bearings(2, 2);
                    bearing4 = box_bearings(2, 1);
%                     [ ~, bearing1, ~ ] = dist_wh_math( [ src_lat grid_lat( 1 ) ], [ src_lon grid_lon( 1 ) ] );
%                     [ ~, bearing2, ~ ] = dist_wh_math( [ src_lat grid_lat( 1 ) ], [ src_lon grid_lon( 2 ) ] );
%                     [ ~, bearing3, ~ ] = dist_wh_math( [ src_lat grid_lat( 2 ) ], [ src_lon grid_lon( 2 ) ] );
%                     [ ~, bearing4, ~ ] = dist_wh_math( [ src_lat grid_lat( 2 ) ], [ src_lon grid_lon( 1 ) ] );

                    if     bearing1 >=  90 && bearing1 < 180   % Q2
                       branch( 2 ) = bearing1;
                    elseif bearing1 >= 270 && bearing1 < 360   % Q4
                       branch( 1 ) = bearing1;
                    end

                    if     bearing2 >=   0 && bearing2 <  90   % Q1
                       branch( 1 ) = bearing2;
                    elseif bearing2 >= 180 && bearing2 < 270   % Q3
                       branch( 2 ) = bearing2;
                    end

                    if     bearing3 >=  90 && bearing3 < 180   % Q2
                       branch( 1 ) = bearing3;
                    elseif bearing3 >= 270 && bearing3 < 360   % Q4
                       branch( 2 ) = bearing3;
                    end

                    if     bearing4 >=   0 && bearing4 <  90   % Q1
                       branch( 2 ) = bearing4;
                    elseif bearing4 >= 180 && bearing4 < 270   % Q3
                       branch( 1 ) = bearing4;
                    end

                    full_circle = isequal(branch, [0 360]);
                    tlgd.fullcircle = full_circle;
                    
                    % Compute the bearing and range values for all lat, lon grid points
                    
                    [grid_bearing, grid_range] = HLS.dist_grid_math(src_lat, src_lon, lat, lon);
                    % adjust the bearing to be consistent with the branch cut used

                    ii = find( grid_bearing < branch( 1 ) );
                    grid_bearing( ii ) = grid_bearing( ii ) + 360;

                    ii = find( grid_bearing > branch( 2 ) );
                    grid_bearing( ii ) = grid_bearing( ii ) - 360;

                    % save the bearing and range
                    tlgd.grid_range_km    = grid_range / 1000.0;
                    tlgd.grid_bearing_deg = grid_bearing;
                  
                    %%

                    % Determine the min, max values of both the bearing angles and range

                    bearing_min = min( min( tlgd.grid_bearing_deg ) );
                    bearing_max = max( max( tlgd.grid_bearing_deg ) );

                    % Apply any needed tweaks to the min, max of the bearing angles

                    if ( bearing_max - bearing_min ) > 180
                      % calculate over a full disk
                      bearing_min = branch( 1 );
                      bearing_max = branch( 2 );
                    end

                    tlgd.nbearings = round( ( bearing_max - bearing_min ) / Params.bearing_resolution );
                    tlgd.nbearings = max( [ tlgd.nbearings, 2 ] );

                    tlgd.bearings = linspace( bearing_min, bearing_max, ...
                                              tlgd.nbearings + full_circle);
                                          
                    % Apply any needed tweaks to the min, max of the range

                    range_min   = min( min( tlgd.grid_range_km ) );
                    range_max   = max( max( tlgd.grid_range_km ) );

                case 'ENDPOINTS'
                    tlgd.nbearings = 1;

                    rcv_lat = Params.rcv_lat;
                    rcv_lon = Params.rcv_lon;

                    % Compute the bearing of the receiver (relative to src)

                    range_min   = 0;
                    [bearing_deg, grid_range] = HLS.dist_grid_math(src_lat, src_lon, rcv_lat, rcv_lon);
                    tlgd.bearings( 1 ) = bearing_deg;      % bearing of receiver plane (deg cw from true N)
                    range_max = grid_range/1000;           % range in km

                case 'POLAR'
                    tlgd.nbearings = 1;
                    tlgd.bearings( 1 ) = Params.bearing_deg;      % bearing of receiver plane (deg cw from true N)
      
                    range_min   = 0;
                    range_max   = Params.max_r_km;

                otherwise
                    error([ mfilename, ': unrecognized method for specification of computational plane: ', method ]);
            end

            range_resolution_km = Params.range_resolution_km;

            if Params.src_bw > 0.0
              tlgd.alpha = Params.src_bw;

              % case of range averaging (as a surrogate for frequency averaging)
              range_min = ( 1.0 - tlgd.alpha ) * range_min;
              range_max = ( 1.0 + tlgd.alpha ) * range_max;
              del_range = range_max - range_min;

              % range averaging also influences the (required) range resolution
              ra_resolution_km  = 0.5 * tlgd.alpha * ( 0.9 * range_min + 0.1 * range_max ) / 3.0;
              min_resolution_km = min( [ range_resolution_km, ra_resolution_km ] );
              tlgd.nrr          = 1 + round( del_range / min_resolution_km );
            else
              % no range averaging computation
              range_min = max( [ range_min - 0.20, 0.0 ] );
              range_max = range_max + range_resolution_km;
              del_range = range_max - range_min;
              tlgd.nrr  = 1 + round( del_range / range_resolution_km );
            end
            tlgd.nrr = max(tlgd.nrr, 2);

            %%

            % Finish setup of the tl_grid properties

            grid_depths = Params.grid_depths;

            min_d_m  = grid_depths( 1 );
            max_d_m  = grid_depths( 2 );
            tlgd.nrd = grid_depths( 3 );

            Plane.method      = 'POLAR';
            Plane.src_lat     = src_lat;
            Plane.src_lon     = src_lon;
            Plane.min_r_km    = range_min;
            Plane.max_r_km    = range_max;
            Plane.min_d_m     = min_d_m;
            Plane.max_d_m     = max_d_m;
            Plane.nrr         = tlgd.nrr;
            Plane.nrd         = tlgd.nrd;

            tlgd.Plane = Plane;

            src_lat = Plane.src_lat;
            src_lon = Plane.src_lon;
            
            switch upper(Plane.method)
               case 'ENDPOINTS'

                  rcv_lat = Plane.rcv_lat;
                  rcv_lon = Plane.rcv_lon;

                  % Compute the bearing and range of the receiver (relative to src)

                  r_m = dist_wh_math( [src_lat rcv_lat], [src_lon rcv_lon] );

                  min_r_km = 0.0;
                  max_r_km = r_m / 1000.0;

               case 'POLAR'
                  if isfield( Plane, 'min_r_km' )
                     min_r_km = Plane.min_r_km;
                  else
                     min_r_km = 0.0;
                  end

                  max_r_km = Plane.max_r_km;

               otherwise
                  error( [ mfilename, ': unrecognized method for specification of computational plane: ', method ] );
            end

            % Setup the receiver positions on the computational plane

            del_r = max_r_km / Plane.nrr;

            if Plane.nrr == 1 || min_r_km < del_r
                rr = ( 1:Plane.nrr ) * del_r;
            else
                rr = linspace( min_r_km, max_r_km, Plane.nrr );
            end

            del_z = Plane.max_d_m / Plane.nrd;

            if Plane.nrd == 1 || min_d_m < del_z
               rd = ( 1 : Plane.nrd ) * del_z;
            else
               rd = linspace( min_d_m, max_d_m, Plane.nrd );
            end

            tlgd.Pos.Nrr = Plane.nrr;
            tlgd.Pos.Nrd = Plane.nrd;
            tlgd.Pos.r.range = rr;
            tlgd.Pos.r.depth = rd;

            tlgd.Beam.Box.r = max_r_km;

            r_km  = rr;
            dr_km = r_km( 2 ) - r_km( 1 );

            % Computations when range averaging is enabled
            if tlgd.alpha > 0
                % number of points in the half-window for range averaging
                tlgd.no_pts = round( ( 0.5 * tlgd.alpha * r_km ) ./ dr_km );
                tlgd.no_pts = max( tlgd.no_pts, 1 );   % make sure a minimum of 1 point in the average

                % indices where range averaging computation is feasible
                iranges = find( ( ( 1 : tlgd.nrr ) - tlgd.no_pts + 1 ) >= 1 & ...
                              ( ( 1 : tlgd.nrr ) + tlgd.no_pts - 1 ) <= tlgd.nrr );
                tlgd.range_mask = eye(tlgd.nrr);
                for i = iranges
                    tlgd.range_mask((i-tlgd.no_pts(i)+1):(i+tlgd.no_pts(i)-1),...
                        i) = true;
                end
                tlgd.range_divisor = ones(1, tlgd.nrr);
                tlgd.range_divisor(iranges) = 2*tlgd.no_pts(iranges) - 1;
            end
        end
        function tlgd = copyprops(tlgd, sl)
            tlgd.Bdry = sl.Bdry;
            tlgd.Beam = sl.Beam;
            tlgd.cInt = sl.cInt;
            tlgd.freq = sl.freq;
            tlgd.Plane = sl.Plane;
            tlgd.Pos = sl.Pos;
            tlgd.RMax = sl.RMax;
            tlgd.SSP = sl.SSP;
            tlgd.SSP_mat = sl.SSP_mat;
            tlgd.SSP_range = sl.SSP_range;
            tlgd.TitleEnv = sl.TitleEnv;
        end
    end
end

