function TLmat = compute(tl_grid)
%HLS.tl_grid.compute calculates a matrix of transmission loss values using
%the HLS.tl_grid object
%   All necessary parameters should be properties of the tl_grid.
    if ~isempty(gcp('nocreate'))   % run in parallel
        spmd
            TLpolar_d = codistributed.zeros(tl_grid.nrd, tl_grid.nrr, tl_grid.nbearings, ...
                                      codistributor1d(3));
            angles = codistributed.colon(1, tl_grid.nbearings, codistributor1d);
            l_angles = getLocalPart(angles);
            for iangle = l_angles
              disp( [ 'iangle = ' num2str( iangle ), ' of ', num2str( tl_grid.nbearings ) ] )

              tl_grid.Plane.bearing_deg = tl_grid.bearings( iangle );

              % *** Run the acoustic propagation model on the Slice ***

              [tl_db, rc] = tl_grid.SingleSlice();
              if isa(tl_db, 'AT.tlgrid')
                  tl_db = tl_db.tlg;
              end

              if ~isempty(strfind(tl_grid.acoustic_model, 'MEX')) && ~isempty(tl_db)
                  rc = 0;  % Throw away print file (or add code here to save it)
              end
          
              if rc ~= 0   % run went OK?
                error( [ mfilename, ': execution of acoustic model (', tl_grid.acoustic_model, ...
                         ') returned error code: ', num2str( rc ) ] );
              end

              % Perform range averaging to approximate averaging over acoustic frequency

              if tl_grid.alpha > 0.0
                tl = 10.0 .^ ( tl_db / -20.0 );  % convert TL from dB to linear units

                % compute range averages for each depth
                tl_bar = bsxfun(@rdivide, tl(:, 1:tl_grid.nrr)*tl_grid.range_mask, tl_grid.range_divisor);

                tl_db = -20.0 * log10( tl_bar );   % convert the range averaged TL to dB
              end

              % need to specify 1 : nrr below because RAM may return an extra point
              TLpolar_d( :, :, iangle ) = tl_db( :, 1:tl_grid.nrr );   % Store this slice for later spatial interpolation
            end
        end
        TLpolar = gather(TLpolar_d);
        tl_grid = tl_grid{1};
    else
        TLpolar = zeros(tl_grid.nrd, tl_grid.nrr, tl_grid.nbearings);
        for iangle = 1 : tl_grid.nbearings
          disp( [ 'iangle = ' num2str( iangle ), ' of ', num2str( tl_grid.nbearings ) ] )

          tl_grid.Plane.bearing_deg = tl_grid.bearings( iangle );

          % *** Run the acoustic propagation model on the Slice ***

          [tl_db, rc] = tl_grid.SingleSlice();
          if isa(tl_db, 'AT.tlgrid')
              tl_db = tl_db.tlg;
          end

          if ~isempty(strfind(tl_grid.acoustic_model, 'MEX')) && ~isempty(tl_db)
              rc = 0;  % Throw away print file (or add code here to save it)
          end
          
          if rc ~= 0   % run went OK?
            error( [ mfilename, ': execution of acoustic model (', tl_grid.acoustic_model, ...
                     ') returned error code: ', num2str( rc ) ] );
          end

          % Perform range averaging to approximate averaging over acoustic frequency

          if tl_grid.alpha > 0.0
            tl = 10.0 .^ ( tl_db / -20.0 );  % convert TL from dB to linear units

            % compute range averages for each depth
            tl_bar = bsxfun(@rdivide, tl(:, 1:tl_grid.nrr)*tl_grid.range_mask, tl_grid.range_divisor);

            tl_db = -20.0 * log10( tl_bar );   % convert the range averaged TL to dB
          end

          % need to specify 1 : nrr below because RAM may return an extra point
          TLpolar( :, :, iangle ) = tl_db( :, 1:tl_grid.nrr );   % Store this slice for later spatial interpolation
        end
    end

    % For each depth, interpolate from the polar grid to the caller's lat, lon grid

    if tl_grid.fullcircle   % duplicate 0 angle as 360
        TLpolar = cat(3, TLpolar, TLpolar(:, :, 1));
    end
    if tl_grid.nbearings == 1
        TLmat = TLpolar(:, :, 1);
        return
    end
    [nlat, nlon] = size(tl_grid.grid_range_km);
    TLmat = zeros( nlat, nlon, tl_grid.nrd );

    for idepth = 1 : tl_grid.nrd
      % interpolate to the exact bearing and range of this lat, lon point
      TLmat( :, :, idepth ) = interp2( tl_grid.bearings, tl_grid.Pos.r.range, ...
                                       squeeze( TLpolar(idepth, :, : ) ), ...
                                       tl_grid.grid_bearing_deg, tl_grid.grid_range_km );
    end
end
