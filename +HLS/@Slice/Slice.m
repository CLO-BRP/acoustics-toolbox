classdef Slice
    %Slice - object for calculation of a slice
    %   By default it is configured to do a BELLHOP calculation with
    %   Pekeris sound speed profile and water depth of 1000 meters.

    properties
        acoustic_model = 'BELLHOP'
        bathy_delta_km = 0.05
        bathy_interp   = 'C'
        bathy_rd       = []
        Bdry           = struct('Top', struct([]), 'Bot', struct([]))
        Beam           = struct('alpha', [], 'Box', struct([]), 'deltas', 0.0,...
                                'Nbeams', 0, 'RunType', '')
        cInt           = struct('Low', 1500.0, 'High', 1.0d+09)
        freq           = 5000.0
        Plane          = struct('specify_method', 'POLAR', 'min_r_km', 0.0,...
                                'max_r_km', 0.0, 'min_d_m', 0.0, 'max_d_m', 0.0,...
                                'nrd', 0, 'nrr', 0)
        Pos            = struct('Nrd', 0, 'Nrr', 0, 'Nsd', 1, 'r', struct([]),...
                                's', struct([]))
        RMax           = 10.1
        SSP            = struct('NMedia', 1, 'N', 0, 'sigma', 0.0, 'depth', [],...
                                'raw', struct([]))
        SSP_mat        = []
        SSP_range      = []
        TitleEnv       = 'Default Slice'        
    end

    methods
        function sl = Slice() % constructor
            Top.HS = struct([]);
            Top.Opt = 'CVFT  ';	% 'C' C-linear interpolation of sound speed profile
                        % 'V' Vacuum above top, air-sea interface (open ocean)
                        % 'F' Frequency dependent attenuation, dB/(kmHz)
                        % 'T' Thorp volume attenuation formula
                        % ' ' Flat air-sea interface (no *.ati altimetry file)
                        % ' ' Trace all the rays in the beam fan

            HS.alphaR = 1600.0;
            HS.betaR  = 0.00;
            HS.rho    = 1.50;
            HS.alphaI = 0.75;
            HS.betaI  = 0.00;

            Bot.alphaR = 1600.0;
            Bot.betaR  = 0.00;
            Bot.rho    = 1.50;
            Bot.alphaI = 0.75;
            Bot.betaI  = 0.00;
            Bot.HS     = HS;
            Bot.Opt    = 'A*';	% Acousto-Elastic bottom, read in *.bty file

            sl.Bdry.Top = Top;
            sl.Bdry.Bot = Bot;

            Box.r = 10.1;			% Maximum range for tracing rays (km)
            Box.z = 1001.0;			% Maximum depth for tracing rays  (m)

            sl.Beam.alpha   = [ -45.0, 45.0 ];	% Beam fan angles (angles < 0, toward surface)
            sl.Beam.Box     = Box;
            sl.Beam.Nbeams  = 0;		% Number of rays in the source beam fan
            sl.Beam.RunType = 'CB R';   % Coherent TL calculation, Gaussian beams

            sl.Plane.min_r_km       = 0.0;      % Minimum range of receivers in km
            sl.Plane.max_r_km       = 10.0;     % Maximum range of receivers in km
            sl.Plane.min_d_m        = 0.0;      % Minimum depth of receivers in meters
            sl.Plane.max_d_m        = 1000.0;	 % Maximum depth of receivers in meters
            sl.Plane.nrd            = 50;       % Number of receivers in depth dimension
            sl.Plane.nrr            = 40;       % Number of receivers in range dimension

            r.depth = sl.Plane.max_d_m  * ( 1 : sl.Plane.nrd ) / sl.Plane.nrd;
            r.range = sl.Plane.max_r_km * ( 1 : sl.Plane.nrr ) / sl.Plane.nrr;
            s.depth = 50.0;

            sl.Pos.Nrd = sl.Plane.nrd;
            sl.Pos.Nrr = sl.Plane.nrr;
            sl.Pos.r   = r;
            sl.Pos.s   = s;

            raw( 1 ).z      = [    0.0 1000.0 ];	% Depth, values > 0 are underwater
            raw( 1 ).alphaR = [ 1500.0 1500.0 ];	% P-wave phase velocity (m/s)
            raw( 1 ).betaR  = [    0.0    0.0 ];	% S-wave phase velocity (m/s)
            raw( 1 ).rho    = [    1.0    1.0 ];	% Fluid density (g/cm^3)
            raw( 1 ).alphaI = [    0.0    0.0 ];	% P-wave attenuation (Top.Opt(3) units)
            raw( 1 ).betaI  = [    0.0    0.0 ];	% S-wave attenuation (Top.Opt(3) units)

            sl.SSP.depth  = [0.0 1000.0];
            sl.SSP.raw    = raw;
        end
    end
end
