% RUN_SLICER_RAM: Script to run MATLAB RAM on same model as the original.

% Set up Params struct
Params.BathyFile = 'Stellwagenxyz.mat';
Params.EnvFile = 'Stellwagen.env';
Params.acoustic_model = 'RAM';
Params.RunType = 'ENDPOINTS';
Params.src_depth = 25;
Params.src_freq = 250;
Params.src_bw = 0;
Params.Bot.alphaR = 1593;
Params.Bot.betaR = 0;
Params.Bot.rho = 1.2200;
Params.Bot.alphaI = 0.592;
Params.Bot.betaI = 0;
Params.rcv_lat = 42.7200;
Params.rcv_lon = -70.599;
Params.range_resolution_km= 0.02;
Params.grid_depths = [0 300 200];
Params.src_lat = 42.6300;
Params.src_lon = -70.6000;

%Instantiate the tl_grid
tl_grid = HLS.tl_grid(Params);
TLmat = tl_grid.compute;
profile off

% plot TL

rcv_ranges = tl_grid.Pos.r.range;
rcv_depths = tl_grid.Pos.r.depth;

figure
imagesc( rcv_ranges, rcv_depths, TLmat )
title( 'RAM example for Stellwagen Bank - Coherent TL');
xlabel( 'Range (km)' )
ylabel( 'Depth (m)' )

tej = flipud( jet( 256 ) );  % 'jet' colormap reversed
colormap( tej )
AT.caxisrev( [ 30 100 ] )