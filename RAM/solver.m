function u = solver(u, r1, r2, r3, iz)
% Function to solve tridiagonal matrix to be compiled into a mexfunction
    nz = length(r1);
    for i = 3:iz
        u(i) = u(i) - r1(i-1)*u(i-1);
    end
    for i = nz:-1:iz+2
        u(i) = u(i) - r3(i+1)*u(i+1);
    end
    u(iz+1) = (u(iz+1) - r1(iz)*u(iz) - r3(iz+2)*u(iz+2))*r2;
    for i = iz:-1:2
        u(i) = u(i) - r3(i+1)*u(i+1);
    end
    for i = iz+2:nz+1
        u(i) = u(i) - r1(i-1)*u(i-1);
    end
end